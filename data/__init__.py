from readfile import STATIC_FILES

CENSUSES =  tuple(range(1, 8))

SCALING_FACTORS =   (1.5, 1.6, 1.65, 1.7, 1.75,
                    1.8, 1.85, 1.9, 1.95, 1.990,
                    1.995, 1.999, 2., 2.05, 2.10,
                    2.15, 2.25, 2.5)
