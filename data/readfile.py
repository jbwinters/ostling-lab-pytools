from pandas.io import parsers
import os.path
import numpy as np
import sys

STATIC_FILES = os.path.normpath(os.environ['STATIC_FILES'])
def filename_generator(numruns, fileprefix, filepost, start_index=0):
    for i in range(start_index,numruns+start_index):
        yield (i, "{0}{1}{2}".format(fileprefix,str(i),filepost))

def full_filename_generator(rundir, numruns, fileprefix, filepost,
        start_index=0):
    rundir = os.path.normpath(rundir)
    fname_gen = filename_generator(numruns, fileprefix, filepost, start_index)
    for i, fname in fname_gen:
        yield i, rundir + '/' + fname

def rundir_gen(multirundir):
    for line in sys.stdin:
        theta,m=line.split()
        dirname="theta_{0}-m_{1}".format(theta,m)
        full_path = os.path.normpath(os.path.join(multirundir, dirname))
        yield (full_path, theta, m)

def abund_file(filepath):
    names = ['sp_id', 'sp_count', 'sp_diam', 'sp_diam_pow']
    return parsers.read_table(filepath, sep=' ', header=None,
            index_col=0, names=names)

def abund_files(rundir, numruns, start_index=0):
    abunds = []
    for _, fname in full_filename_generator(rundir, numruns,
            'Abund_', '_1.txt', start_index=start_index):
        abunds.append(abund_file(fname))
    return abunds

def individual_file(filepath, four_cols=False):
    if not four_cols:
        names = ['sp_id', 'size_class', 'diam', 'diam_pow', 'num_individuals']
    else:
        names = ['sp_id', 'size_class', 'diam', 'num_individuals']
    return parsers.read_table(filepath, sep=' ', header=None, names=names,
            index_col=0)

def individual_files(rundir, numruns, start_index=0, four_cols=False):
    indivs = []
    for _, fname in full_filename_generator(rundir, numruns,
            'Individuals_', '_1.txt', start_index=start_index):
        try:
            indivs.append(individual_file(fname, four_cols=four_cols))
        except:
            print 'Error reading:', fname
            sys.exit(1)
    return indivs

def logSAD_file(filepath):
    names = ['bins']
    return parsers.read_table(filepath, sep=' ', header=None, names=names)

def logSAD_files(rundir, numruns, start_index=0):
    logSADs = []
    for _, fname in full_filename_generator(rundir, numruns, 'logSAD_',
            '_1.txt', start_index=start_index):
        logSADs.append(logSAD_file(fname))
    return logSADs

def observed_bci(census=4):
    obs_file = os.path.join(    STATIC_FILES,
                                'observed_data',
                                'bci_other_years',
                                'bci%d_main_dead.txt' % census)
    obs_data = parsers.read_table(obs_file, sep='\t', index_col=1)
    return obs_data

def logSAD_bci():
    obs_file = os.path.join(    STATIC_FILES,
                                'observed_data',
                                'logSAD_BCI.txt')
    obs_data = parsers.read_table(obs_file, header=None, names=['bins'])
    return obs_data['bins']

def observed_bci_file(census=4, base=2.0):
    obs_file = os.path.join(    STATIC_FILES,
                                'observed_data',
                                'bci_other_years',
                                'census%d' % census,
                                'base_%0.3f.txt' % base)
    obs_data = parsers.read_table(obs_file, header=None, names=['bins'])
    return obs_data['bins']

def observed_size_distribution():
    obs_file = os.path.join(    STATIC_FILES,
                                'observed_data',
                                'bci_size_distribution.txt')
    obs_data = parsers.read_table(obs_file, header=None, sep=' ',
            names=['binned size class', 'num_individuals'], index_col=0)
    return obs_data['num_individuals']
