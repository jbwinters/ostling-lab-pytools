import sys
import numpy as np
from pandas.io import parsers
from data import readfile, SCALING_FACTORS, STATIC_FILES, CENSUSES
from analysis import species_biomass
import os.path
from scipy import stats
from collections import namedtuple, defaultdict
import matplotlib.pyplot as plt
import operator
from math import floor

# array.argmin, array.argmax for index of min, max
TROUGH_PEAK_DIFF_FACTOR = 1.20

def higher_value_in_array(array, low_val):
    if not len(array):
        return None
    max_val = np.max(array)
    if max_val <= low_val:
        return None
    return max_val, array.argmax()

def remove_zeros(array):
    return array[array != 0]

def find_side_extrema(array, twenty_percent_of_species=None):
    # for i in range(blah, -1):
    # if has_higher() and in correct range:
    #   compare to min, replace min if smaller
    #ten_percent_of_species = 22. ###
    array = remove_zeros(array)
    min_val = 1000000000
    next_higher_val = None
    for i, val in enumerate(array[:-2]):
        if twenty_percent_of_species \
            and sum(array[i + 1:]) < twenty_percent_of_species:
                break
        higher_val = higher_value_in_array(array[i + 1:], val)
        if higher_val and val < min_val:
            if higher_val[0] > TROUGH_PEAK_DIFF_FACTOR * val:
                min_val = val
                next_higher_val = higher_val
    if next_higher_val:
        return min_val, next_higher_val[0]
    raise ValueError('Necessary pattern could not be found')

def difference(a, b):
    return (a - b)

def sorted_indexes_by_value(array):
    sorted_array = sorted([(v, i) for i, v in enumerate(array)], reverse=True)
    sorted_indexes = [i for v, i in sorted_array]
    return sorted_indexes

def sum_extrema_differences(array, tmp_obs=None):
    if not len(array):
        return 0
    sorted_indexes = sorted_indexes_by_value(array)
    gmax_i = sorted_indexes.pop(0)
    found = False
    counter = 0
    twenty_percent_of_species = floor(sum(array) * 0.20)

    while counter < 3 and not found:
        try:
            if sum(array[:gmax_i + 1]) < 0.4 * sum(array):
                raise ValueError()
            right_low, right_high = find_side_extrema(array[gmax_i + 1:],
                    twenty_percent_of_species=twenty_percent_of_species)
            found = True
        except ValueError:
            counter += 1
            gmax_i = sorted_indexes.pop(0)
    if not found:
        return 0

    gmax = array[gmax_i]

    diff_gmax_right = difference(gmax, right_low)
    diff_right = difference(right_high, right_low) ** 2

    sum_diffs = diff_gmax_right + diff_right
    #if tmp_obs and sum_diffs > tmp_obs:
        #print sum_diffs, array[array != 0]
        #print left_high, left_low, gmax, right_low, right_high
    return sum_diffs

def relative_frequencies(array):
    return array / sum(array)

def mm_diffs(rundir, numruns, base,
        fname_start_index=0, census=4, rel_freqs=False):
    _, each_run_dists_pow = species_biomass.\
            calculate_each_run_distribution(rundir, numruns, base=base,
            fname_start_index=fname_start_index)

    observed_dist_pow = readfile.observed_bci_file(census=census, base=base)
    #print 'obs dist:', observed_dist_pow
    if rel_freqs:
        each_run_dists_pow = [relative_frequencies(dist)
                for dist in each_run_dists_pow]
        observed_dist_pow = relative_frequencies(observed_dist_pow)

    obs_sum = sum_extrema_differences(observed_dist_pow)
    #print 'obs metric:', obs_sum

    run_sums = []
    for run_dist in each_run_dists_pow:
        run_sum = sum_extrema_differences(run_dist, tmp_obs=obs_sum)
        run_sums.append(run_sum)
        #if run_sum > obs_sum:
            #print 'run dist:', run_dist
            #print 'run metric:', run_sum

    run_sums = np.array(run_sums)
    #return np.mean(run_sums)
    #print 'sums', run_sums
    #print 'len zeros', len(run_sums[run_sums == 0])
    #print 'gt obs', len(run_sums[run_sums > obs_sum]) / float(len(run_sums))
    return len(run_sums[run_sums > obs_sum]) / float(len(run_sums)), \
            np.mean(run_sums)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt'\
                % __file__
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    bases = SCALING_FACTORS
    #bases = (2.,)

    print ';'.join(['Theta', 'm-rate', 'Fraction'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        for base in bases:
            results = "%s;%s;%f;" % (theta, m, base)
            for census in CENSUSES:
                print >>sys.stderr, theta, m, base, census
                diff_fraction, chisq_mean = mm_diffs(rundir + '/output',
                        numruns, base, rel_freqs=False, census=census)
                results += "%f;%f;" % (chisq_mean, diff_fraction)
            print results
            sys.stdout.flush()
