import sys
import numpy as np
from pandas.io import parsers
from data import readfile
from analysis import species_biomass
import os.path
from scipy import stats

import matplotlib.pyplot as plt

def two_point_slope(bins, index1, index2):
    assert(index2 != index1)
    v1 = 0
    v2 = 0
    if len(bins) - 1 >= index2:
        v1 = bins[index1]
        v2 = bins[index2]
    elif len(bins) - 1 >= index1:
        v1 = bins[index1]
    return (v2 - v1) / (index2 - index1)

def regression_slope(bins, start_i, end_i):
    if (len(bins) - 1) - start_i < 3:
        return two_point_slope(bins, start_i, len(bins) - 1)
    return stats.linregress(np.arange(end_i - start_i), bins[start_i:end_i])[0]

def slope(dist, start_i, end_i):
    if end_i - start_i > 1:
        slope = regression_slope(dist, start_i, end_i + 1)
    else:
        slope = two_point_slope(dist, start_i, end_i)
    return slope

def slope_fractions(obs_dist, each_run_dists, start_i, mid_i, end_i):
    slope_start_mid = slope(obs_dist, start_i, mid_i)
    slope_mid_end = slope(obs_dist, mid_i, end_i)
    obs_diff = slope_mid_end - slope_start_mid
    #print >>sys.stderr, 'obs', obs_diff

    slope_diffs = []
    for i, run_dist in enumerate(each_run_dists):
        #print run_dist
        #plt.plot(range(len(obs_dist)), obs_dist, 'b-')
        #plt.plot(range(len(run_dist)), run_dist, 'g-')
        #plt.savefig('../output/regression_fractions_2.50/%d.png' %i)
        #plt.clf()
        slope_start_mid = slope(run_dist, start_i, mid_i)
        slope_mid_end = slope(run_dist, mid_i, end_i)
        slope_diffs.append(slope_mid_end - slope_start_mid)
    slope_diffs = np.array(slope_diffs)
    #print >>sys.stderr, 'slopes', slope_diffs.min(), slope_diffs.mean(), sorted(slope_diffs)[len(slope_diffs)/2], slope_diffs.max()

    run_diffs_gt_obs_diff = len(slope_diffs[slope_diffs > obs_diff])
    return run_diffs_gt_obs_diff / float(len(slope_diffs))

STATIC_FILES = os.path.normpath(os.environ['STATIC_FILES'])

def compare_slopes(rundir, numruns, base, start_i, mid_i, end_i,
        fname_start_index=0):
    each_run_dists, each_run_dists_pow = species_biomass.\
            calculate_each_run_distribution(rundir, numruns, base=base,
            fname_start_index=fname_start_index)


    #log2_dist = os.path.join(STATIC_FILES,
            #'observed_data/log2_species_biomass_distribution.txt')
    #log2_dist_pow = os.path.join(STATIC_FILES,
            #'observed_data/log2_species_biomass_distribution_pow8-3.txt')
    log_dist_pow = os.path.join(STATIC_FILES, 'observed_data',
            'observed_species_biomass_distributions',
            'base_%0.3f.txt' % base)
    #observed_dist = parsers.read_table(log2_dist, names=['bins'], dtype=np.float)['bins']
    observed_dist_pow = parsers.read_table(log_dist_pow,
            names=['bins'], dtype=np.float)['bins']
    #return (slope_fractions(observed_dist, each_run_dists),)
            #slope_fractions(observed_dist_pow, each_run_dists_pow))

    return slope_fractions(observed_dist_pow, each_run_dists_pow,
            start_i, mid_i, end_i)

if __name__ == '__main__':
    if len(sys.argv) != 7:
        print 'Use: python %s <multirundir> <numruns> <log base> <start index> <mid index> <end index> < param_pairs.txt'\
                % __file__
        print '(Use zero-based indexes)'
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    base = float(sys.argv[3])
    start_i = int(sys.argv[4])
    mid_i = int(sys.argv[5])
    end_i = int(sys.argv[6])

    print ';'.join(['Theta', 'm-rate', 'Regression fraction'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m
        #if theta=='44' and m=='0.21':
            #reg_frac, reg_frac_pow = compare_slopes(rundir + '/output', numruns)
        reg_frac = compare_slopes(rundir + '/output', numruns,
                base, start_i, mid_i, end_i)
        reg_frac_pow = 0.
        print "%s;%s;%f" % (theta, m, reg_frac)
        sys.stdout.flush()
