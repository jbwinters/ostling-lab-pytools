import os, sys
from math import exp
from collections import namedtuple

import numpy as np
import pandas as pd
from matplotlib import rc

from data import readfile, STATIC_FILES
from analysis.plot import ParamPair
from analysis import plot, species_biomass, size_distribution

FEC_SCALING_RUNS = os.path.normpath(os.environ['FECUNDITY_SCALING_RUNS'])
MULTIRUN3 = os.path.normpath(os.environ['MULTIRUN3'])
HYBRID_WEIBULL = os.path.normpath(os.environ['HYBRID_OBSERVED_WEIBULL'])

def trim_zero_observed(array, nonzeros):
    return array[nonzeros]

class SpeciesAbundanceDistributionFigure(plot.Figure):
    def build_figure(self):
        bci = self.BCI_abundance_distribution()
        bci_not_0 = bci != 0
        bci = trim_zero_observed(bci, bci_not_0)
        bci_plot = self.ax.bar(range(len(bci)), bci,
                align='center', facecolor='#dddddd',
                edgecolor='#aaaaaa', label='BCI')

        best_fit_neutral = np.array([9.3844, 15.4365, 19.1834, 23.7080, 27.8825,
                30.0286, 29.4228, 26.7539, 21.9431, 14.8264, 6.8972, 1.5922])
        best_fit_neutral = trim_zero_observed(best_fit_neutral, bci_not_0)
        orig_neutral_plot = self.ax.plot(range(len(best_fit_neutral)),
                best_fit_neutral, 'k^-',
                label='Original neutral\nbest fit')

        self.labelspacing = 0
        self.figure_specific(bci_len=len(bci))

        self.ax.set_xticks(np.arange(len(bci)))
        xtick_labels = ["{:,d}".format(2 ** i) for i in range(len(bci))]
        self.ax.set_xticklabels(xtick_labels, fontsize='x-small', rotation=30)

        self.ax.set_xlim(left=-1, right=12)
        self.ax.set_ylim(top=38)

        plots = [orig_neutral_plot, bci_plot]
        plots = [plot[0] for plot in plots]
        plot_labels = ['Original neutral\nbest fit', 'BCI']
        self.add_legend(plots, plot_labels,
                prop={'size': 6}, frameon=False, loc='upper left',
                labelspacing=self.labelspacing)
        self.set_xlabel("Number of individuals (log$\mathsf{_2}$ scale)")
        self.set_ylabel("Number of species")

    def BCI_abundance_distribution(self):
        return readfile.logSAD_bci()

    def figure_specific(self, **kw):
        raise NotImplementedError()

class ReproductionScalingSADFigure(SpeciesAbundanceDistributionFigure):
    def figure_specific(self, **kw):
        self.labelspacing = 1.95
        bci_len = None
        if 'bci_len' in kw:
            bci_len = kw['bci_len']
        plots = []
        for fec in range(1, 4):
            path = os.path.join(FEC_SCALING_RUNS, str(fec), 'output')
            fec_scaling_logSAD = readfile.logSAD_files(path, 1000)
            mean_logSAD = sum(fec_scaling_logSAD) \
                            / float(len(fec_scaling_logSAD))
            mean_logSAD = mean_logSAD[0:bci_len]
            fec_plot =self.ax.plot(range(len(mean_logSAD)), mean_logSAD,
                    '.-', label=r'$\mathsf{b(D) \sim D^%d}$' % fec,
                    color=self.colors[fec - 1])
            plots.append(fec_plot)
        plots = [plot[0] for plot in plots]
        plot_labels = [plot.get_label() for plot in plots]
        self.add_legend(plots, plot_labels,
                prop={'size':6}, frameon=False, loc='upper right')
                #labelspacing=0.0)

class BestFitSADFigure(SpeciesAbundanceDistributionFigure):
    def figure_specific(self, **kw):
        self.labelspacing = 2.55
        use_legend = True

        use_55 = True
        parameters = []
        parameters.append(ParamPair("44", "0.22", self.colors[1],
            label='$\mathsf{b(D) \sim D^2}$\nbest fit',
            plotstyle=('-' if use_55 else '.-', (None, None))))
        if use_55:
            parameters.append(ParamPair("55", "0.0825", self.colors[1],
                '$\mathsf{b(D) \sim D^2}$\nrealistic m\nbest fit',
                plotstyle=('--', (3, 3))))
        bci_len = None
        if 'bci_len' in kw:
            bci_len = kw['bci_len']

        plots = []
        for i, params in enumerate(parameters):
            rundir = plot.param_pair_to_directory_name(params,
                    full_path=MULTIRUN3)
            logSADs = readfile.logSAD_files(rundir, 1000)
            mean_logSAD = sum(logSADs) / float(len(logSADs))
            mean_logSAD = mean_logSAD[0:bci_len]
            param_plot = self.ax.plot(range(len(mean_logSAD)), mean_logSAD,
                    params.plotstyle[0], color=params.color, label=params.label,
                    dashes=params.plotstyle[1])
            plots.append(param_plot)
        if use_legend:
            plots = [p_plot[0] for p_plot in plots]
            plot_labels = [p_plot.get_label() for p_plot in plots]
            self.add_legend(plots, plot_labels,
                    prop={'size': 6}, frameon=False, loc='upper right',
                    labelspacing=1.0, numpoints=1 if use_55 else 2)

class SpeciesBiomassDistributionFigure(plot.Figure):
    def build_figure(self):
        bci = self.BCI_biomass_distribution()
        bci_not_0 = bci != 0
        #bci = trim_zero_observed(bci, bci_not_0)
        bci_plot = self.ax.bar(range(len(bci)), bci,
                align='center', facecolor='#dddddd',
                edgecolor='#aaaaaa', label='BCI')

        bci_len = len(bci)
        self.parameters = []
        self.add_parameters()

        self.use_legend = True
        plots = []
        for i, params in enumerate(self.parameters):
            rundir = plot.param_pair_to_directory_name(params,
                    full_path=MULTIRUN3)
            _, sbds = species_biomass.\
                    calculate_each_run_distribution(rundir, 1000)
            mean_sbd = sum(sbds) / float(len(sbds))
            mean_sbd = mean_sbd[9:26]
            lines = self.ax.plot(range(9, 26), mean_sbd, params.plotstyle[0],
                    label=params.label if params.label else '',
                    color=params.color)
            if params.plotstyle[0] == '--' or params.plotstyle == ':':
                lines[0].set_dashes(params.plotstyle[1])
            plots.append(lines)

        self.ax.set_xticks(np.arange(len(bci)))
        xtick_labels = ["$\mathsf{2^{%d}}$" % i if i % 2 else ""
                         for i in range(len(bci))]
        self.ax.set_xticklabels(xtick_labels)

        self.ax.set_xlim(left=8.5, right=25.5)
        self.ax.set_ylim(top=33)
        self.set_xlabel("Species biomass (log$\mathsf{_2}$ scale)")
        self.set_ylabel("Number of species")

        if self.use_legend:
            plots = [p_plot[0] for p_plot in plots]
            plot_labels = [p_plot.get_label() for p_plot in plots]
            #num_points - hack
            num_points = 1
            if len(plots) == 1:
                num_points = 2
            self.add_legend(plots, plot_labels,
                    prop={'size': 6}, frameon=False, loc='upper left',
                    labelspacing=1.0, numpoints=num_points)
        self.add_legend([bci_plot], [bci_plot.get_label()],
                prop={'size': 6}, frameon=False, loc='upper right',
                borderpad=1.2)

    def BCI_biomass_distribution(self):
        return readfile.observed_bci_file()

    def add_parameters(self):
        raise NotImplementedError()

class BestFitSBDFigure(SpeciesBiomassDistributionFigure):
    def add_parameters(self):
        self.use_legend = False
        self.parameters.append(ParamPair("40", "0.35", self.colors[1],
                label='$\mathsf{b(D) \sim D^2}$\nbest fit SBD',
                plotstyle=('.-', (None, None))))

class RealisticParameterSBDFigure(SpeciesBiomassDistributionFigure):
    def add_parameters(self):
        self.parameters.append(ParamPair("40", "0.35", self.colors[1],
                plotstyle=('-', None),
                label='$\mathsf{b(D) \sim D^2}$\nbest fit SBD'))
        self.parameters.append(ParamPair("44", "0.22", self.colors[1],
                plotstyle=('--', (1, 1)),
                label='$\mathsf{b(D) \sim D^2}$\nbest fit SAD'))
        self.parameters.append(ParamPair("55", "0.0825", self.colors[1],
                plotstyle=('--', (3, 3)),
                label='$\mathsf{b(D) \sim D^2}$\nbest fit SAD\nrealistic m'))

class SubPlotSBDFigure(RealisticParameterSBDFigure):
    def build_figure(self, num_subplots=5):
        self.use_legend = False
        axis_list = [self.ax]
        for i in range(1, num_subplots):
            axis_list.append(self.add_subplot(num_subplots, 1, i + 1))

        for i, axis in enumerate(axis_list):
            self.ax = axis
            self.build_subplot(i + 1 + 0)
                    # i + 1 because first two Abund files are the same
        self.set_xlabel("Species biomass (log$\mathsf{_2}$ scale)")

    def build_subplot(self, runnum, numruns=60):
        bci = self.BCI_biomass_distribution()
        bci_not_0 = bci != 0
        #bci = trim_zero_observed(bci, bci_not_0)
        bci_plot = self.ax.bar(range(len(bci)), bci,
                align='center', facecolor='#dddddd',
                edgecolor='#aaaaaa', label='BCI')

        bci_len = len(bci)
        self.parameters = []
        self.add_parameters()

        plots = []
        for i, params in enumerate(self.parameters):
            rundir = plot.param_pair_to_directory_name(params,
                    full_path=MULTIRUN3)
            _, sbds = species_biomass.\
                    calculate_each_run_distribution(rundir, numruns)
            mean_sbd = sbds[runnum]
            mean_sbd = mean_sbd[9:26]
            lines = self.ax.plot(range(9, 26), mean_sbd, params.plotstyle[0],
                    label=params.label if params.label else '',
                    color=params.color)
            if params.plotstyle[0] == '--' or params.plotstyle == ':':
                lines[0].set_dashes(params.plotstyle[1])
            plots.append(lines)

        self.ax.set_xticks(np.arange(len(bci)))
        xtick_labels = ["$\mathsf{2^{%d}}$" % i if i % 2 else ""
                         for i in range(len(bci))]
        self.ax.set_xticklabels(xtick_labels)

        self.ax.set_xlim(left=8.5, right=25.5)
        self.ax.set_ylim(top=33)
        self.set_ylabel("Number of species")

        if self.use_legend:
            plots = [p_plot[0] for p_plot in plots]
            plot_labels = [p_plot.get_label() for p_plot in plots]
            #num_points - hack
            num_points = 1
            if len(plots) == 1:
                num_points = 2
            self.add_legend(plots, plot_labels,
                    prop={'size': 6}, frameon=False, loc='upper left',
                    labelspacing=1.0, numpoints=num_points)
        #self.add_legend([bci_plot], [bci_plot.get_label()],
                #prop={'size': 6}, frameon=False, loc='upper right',
                #borderpad=1.2)

class ChiSquareFigure(plot.Figure):
    def build_figure(self):
        # read lc_chisquares_sad_and_sbd.csv
        table = pd.io.parsers.read_csv(os.path.join(STATIC_FILES,
                    'figure_data', 'lc_chisquares_sad_and_sbd.csv'), sep=';')
        # remove rows where mean richness < 226 or > 228
        table = self.rows_between(226, 228, table, 'Mean Species Richness')
        # plot theta vs chi square
        self.ax.plot(table['Theta'], self.get_chisq_col(table),
                'o', label=self.get_label(),
                mfc='none', mew=1.5, mec=self.colors[0])


        theta_lt_40 = pd.io.parsers.read_csv(os.path.join(STATIC_FILES,
                    'figure_data', 'chisquares_theta_less_than_40.csv'),
                    sep=';')
        theta_lt_40 = self.rows_between(0, 50, theta_lt_40,
                self.get_column_name())
        lt_40_dfs = [theta_lt_40[theta_lt_40['Theta'] == df]
                for df in theta_lt_40['Theta'].unique()]
        rows = [df[df['Mean Species Richness']
                    == min(df['Mean Species Richness'])]
                for df in lt_40_dfs]
        concat_rows = pd.concat(rows)
        #self.ax.plot(theta_lt_40['Theta'], self.get_chisq_col(theta_lt_40),
        self.ax.plot(concat_rows['Theta'], self.get_chisq_col(concat_rows), 'o',
                mfc='none', mew=1.5, mec=self.colors[2])
                #'.', color=self.colors[2], label=self.get_label())

        ### special markers
        bbox_props = dict(boxstyle="round", fc="#dddddd", ec="#aaaaaa", lw=1)
        best_fit_props = dict(arrowprops=dict(arrowstyle="fancy", lw=0.1,
                            color="#aaaaaa", connectionstyle="arc3"))
        realistic_m_props = dict(arrowprops=dict(arrowstyle="fancy", lw=0.1,
                            color="#aaaaaa", connectionstyle="angle3"))
        sp_rich_props = dict(arrowprops=dict(arrowstyle="fancy", lw=0.1,
                            color="#aaaaaa", connectionstyle="arc3"))
        annotation_kw = dict(
                    xycoords='data', textcoords='data',
                    bbox=bbox_props,
                    fontsize='xx-small'
                    )

        best_fit_props.update(annotation_kw)
        realistic_m_props.update(annotation_kw)
        sp_rich_props.update(annotation_kw)

        #best fit
        min_df = table[self.get_chisq_col(table)
                == min(self.get_chisq_col(table))]

        min_val = self.get_chisq_col(min_df)
        min_val_theta = min_df['Theta']
        self.ax.annotate("best fit",
                    xy=(min_val_theta, min_val),
                    xytext=(min_val_theta - 1,
                        min_val - min_val * self.best_fit_offset),
                    **best_fit_props)

        # realistic m
        theta_55_df = table[table['Theta'] == 55]
        theta_55_df = theta_55_df[theta_55_df['m-rate'] == 0.0825]

        y_val = self.get_chisq_col(theta_55_df)
        self.ax.annotate("realistic m",
                    xy=(55, y_val),
                    xytext=(56, y_val - y_val * self.realistic_m_offset),
                    **realistic_m_props)

        # sp. rich. not achieved
        for i, row in concat_rows.iterrows():
            row_x = row['Theta']
            row_y = self.get_chisq_col(row)
            self.ax.annotate("species rich.\nnot achieved",
                    xy=(row_x, row_y),
                    xytext=(29.5, 38.4),
                    **sp_rich_props)

        self.figure_specific()

        self.set_xlabel("Theta")
        self.set_ylabel(self.get_label())

    def rows_between(self, a, b, table, column):
        less_than_b = table[table[column] <= b]
        ltb_and_greater_than_a = less_than_b[a <= less_than_b[column]]
        return ltb_and_greater_than_a

    def get_chisq_col(self, table):
        raise NotImplementedError()

    def get_label(self):
        raise NotImplementedError()

    def figure_specific(self):
        raise NotImplementedError()

    def get_column_name(self):
        raise NotImplementedError()


class SADChiSquareFigure(ChiSquareFigure):
    def __init__(self, *args, **kw):
        super(SADChiSquareFigure, self).__init__(*args, **kw)
        self.realistic_m_offset = 0.06
        self.best_fit_offset = 0.06

    def get_chisq_col(self, table):
        return table['BCI SAD chi square']
    def get_label(self):
        return "SAD chi square"
    def figure_specific(self):
        self.ax.set_xlim(left=37, right=63)
        self.ax.set_ylim(bottom=10, top=19)
    def get_column_name(self):
        return 'BCI SAD chi square'

class SBDChiSquareFigure(ChiSquareFigure):
    def __init__(self, *args, **kw):
        super(SBDChiSquareFigure, self).__init__(*args, **kw)
        self.realistic_m_offset = 0.05
        self.best_fit_offset = 0.05

    def get_chisq_col(self, table):
        return table['BCI SBD chi square']
    def get_label(self):
        return "SBD chi square"
    def figure_specific(self):
        self.ax.set_xlim(left=27, right=63)
    def get_column_name(self):
        return 'BCI SBD chi square'

class SizeDistributionFigure(plot.Figure):
    def build_figure(self):
        observed_size_distribution = readfile.observed_size_distribution()
        observed_size_distribution = observed_size_distribution \
                / float(sum(observed_size_distribution))
        self.ax.plot(range(len(observed_size_distribution)),
                observed_size_distribution,
                'k.', label='BCI size distribution')

        self.set_mean_size_dist()
        self.msd_label = 'Size-structured neutral prediction'
        mean_size_dist = self.mean_size_dist['lc_bins'] \
                / float(sum(self.mean_size_dist['lc_bins']))
        self.figure_specific(mean_size_dist > 0)
        self.ax.plot(range(len(mean_size_dist)),
                mean_size_dist, '-', color=self.colors[0],
                label=self.msd_label)

        weibull_fit = self.calculate_weibull_fit()[mean_size_dist > 0]
        #print len(mean_size_dist > 0)
        #print mean_size_dist > 0
        self.ax.plot(range(len(weibull_fit)), weibull_fit,
                '-', color='black', label='Weibull distribution fit')


        self.ax.semilogy(basey=10)
        self.set_xlabel("Binned size class (cm)")
        self.set_ylabel("Proportion of individuals")

        self.ax.set_yticklabels([r"$\mathsf{10^{%d}}$" % power
            for power in range(-5, 1)])
        self.ax.set_xticks(np.arange(len(mean_size_dist)))
        self.ax.set_xticklabels(["%d" % (cm * 2 * 5) if (cm % 5 == 0) else ""
            for cm in range(len(mean_size_dist))])

        self.ax.legend(prop={'size': 6}, frameon=False, numpoints=1,
                loc='upper right')

    def calculate_weibull_fit(self):
        mu = 0.62
        nu = 5.4
        Kwp = .00094193202
        beta = nu
        P = lambda d: 1. / Kwp * (d / beta) ** (mu - 1) * exp(-(d / nu) ** mu)
        integ = np.arange(100,2600)/10.
        prediction = [P(a) for a in integ]
        probs = []
        for ind in range(len(integ)/100):
            probs.append(sum(prediction[ind * 100:ind * 100 + 100]))
        pred_freq = [v / float(sum(probs)) for v in probs]
        return np.array(pred_freq)

    def set_mean_size_dist(self):
        raise NotImplementedError()

    def figure_specific(self):
        raise NotImplementedError()

class PowerLawSDFigure(SizeDistributionFigure):
    def figure_specific(self, msd_gt_0):
        #weibull_fit = self.calculate_weibull_fit()[msd_gt_0]
        #self.ax.plot(range(len(weibull_fit)), weibull_fit,
                #'-', color='black', label='Weibull distribution fit')
        self.ax.set_xlim([-0.5,23.5])
    def set_mean_size_dist(self):
        mean_neutral_file = os.path.join(STATIC_FILES,
                        'figure_data',
                        'size_distribution_without_last_bin.txt')
        self.mean_size_dist = pd.io.parsers.read_table(mean_neutral_file,
                header=None, names=['lc_bins', 'hyb_bins'], sep=' ')

class HybridSDFigure(SizeDistributionFigure):
    def figure_specific(self, msd_gt_0):
        self.msd_label = 'Power-law growth'
        mean_hyb_dist = self.mean_size_dist['hyb_bins'] \
                / float(sum(self.mean_size_dist['hyb_bins']))
        self.ax.plot(range(len(mean_hyb_dist)),
                mean_hyb_dist, '-', color=self.colors[1],
                label='Tuned growth')
        self.ax.set_xlim([-0.5,24.5])

    def set_mean_size_dist(self):
        mean_neutral_file = os.path.join(STATIC_FILES,
                        'figure_data',
                        'size_distribution_with_last_bin.txt')
        self.mean_size_dist = pd.io.parsers.read_table(mean_neutral_file,
                header=None, names=['lc_bins', 'hyb_bins'], sep=' ')

if __name__ == '__main__':
    figure_types =  {
                        'sad_rsf': ReproductionScalingSADFigure,
                        'sad_bff': BestFitSADFigure,
                        'sbd_fit': BestFitSBDFigure,
                        'sbd_fit_with_44_55': RealisticParameterSBDFigure,
                        'sbd_ind_runs': SubPlotSBDFigure,
                        'chisq_sad': SADChiSquareFigure,
                        'chisq_sbd': SBDChiSquareFigure,
                        'pl_sd': PowerLawSDFigure,
                        'hyb_sd': HybridSDFigure,
                    }
    if len(sys.argv) == 2 and sys.argv[1].lower() == 'list_types':
        for k, v in figure_types.items():
            print k, v
        exit()
    elif len(sys.argv) < 3:
        print "Usage: python %s <extension> <figure type 1> [optional figure type 2, 3, etc.]" % __file__
        exit()
    output_file = os.path.join("tmp_graphs", "figure_%s.")
    extension = sys.argv[1].lower()
    if extension in ("pdf", "agg", "ps"):
        rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
        rc('text', usetex=True)
    figures_to_make = sys.argv[2:]

    for fig_type in figures_to_make:
        if fig_type not in figure_types:
            print "%s not a known figure type, skipping" % fig_type
            continue
        if fig_type == 'sbd_ind_runs':
            num_subplots = 5
            subplot_id = [num_subplots, 1, 1]
            height = num_subplots * 60
            sad_plot = figure_types[fig_type]([None, plot.mm_to_inches(height)],
                    subplot_id=subplot_id)
            sad_plot.build_figure(num_subplots=num_subplots)
        else:
            sad_plot = figure_types[fig_type]([None, plot.mm_to_inches(100)])
            sad_plot.build_figure()
        if extension != "none":
            sad_plot.savefig(output_file % fig_type + extension)



