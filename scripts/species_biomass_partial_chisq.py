import sys, os
import numpy as np
from scipy import stats
from pandas.io import parsers
from analysis import species_biomass
from data import readfile, STATIC_FILES, SCALING_FACTORS, CENSUSES

def chisquare(observed, expected):
    if len(observed) < len(expected):
        observed = np.append(observed, np.zeros(len(expected) - len(observed)))
    elif len(expected) < len(observed):
        expected = np.append(expected, np.zeros(len(observed) - len(expected)))
    exp_not_0 = expected != 0
    obs = observed[exp_not_0]
    exp = expected[exp_not_0]
    return stats.chisquare(obs, exp)

def increase_list_length(li, length):
    if length > len(li):
        return np.append(li, np.zeros(length - len(li)))
    return li

def partial_chisq_frac(rundir, numruns, indices, base, fname_start_index=0,
        rel_freq=False, census=4):
    # read data
    _, each_run_dists_pow = species_biomass.\
            calculate_each_run_distribution(rundir, numruns, base=base,
            fname_start_index=fname_start_index)

    log_dist_pow = os.path.join(STATIC_FILES, 'observed_data',
            'bci_other_years',
            'census%d' % census,
            'base_%0.3f.txt' % base)
    obs_dist_pow = parsers.read_table(log_dist_pow,
            names=['bins'], dtype=np.float)['bins']

    if rel_freq:
        each_run_dists_pow = [dist / sum(dist) for dist in each_run_dists_pow]
        obs_dist_pow /= sum(obs_dist_pow)

    obs_dist_pow = increase_list_length(obs_dist_pow, max(indices) + 1)

    # chisquare for observed vs mean distribution
    all_diam_dist_pow = sum(each_run_dists_pow) / float(len(each_run_dists_pow))
    all_diam_dist_pow = increase_list_length(all_diam_dist_pow, max(indices) + 1)
    diam_bins = [all_diam_dist_pow[index] for index in indices]
    obs_bins = [obs_dist_pow[index] for index in indices]
    obs_chisq = chisquare(np.array(obs_bins), np.array(diam_bins))[0]

    # chisquare for each run's distribution vs mean distribution
    bin_chisqs_pow = []
    for run in each_run_dists_pow:
        run_distribution = increase_list_length(run, max(indices) + 1)
        compare_bins = [run_distribution[index] for index in indices]
        #run_chisq = stats.chisquare(
                #np.array(compare_bins), np.array(diam_bins))[0]
        run_chisq = chisquare(
                np.array(compare_bins), np.array(diam_bins))[0]
        bin_chisqs_pow.append(run_chisq)

    bin_chisqs_pow = np.array(bin_chisqs_pow)
    fraction_gt_observed = \
            len(bin_chisqs_pow[bin_chisqs_pow > obs_chisq]) / float(len(bin_chisqs_pow))
    return fraction_gt_observed, obs_chisq

def bin_string_to_int_list(bin_string):
    bins = []
    for group in bin_string.split(','):
        if ':' in group:
            start, end = group.split(':')
            bins += range(int(start), int(end) + 1)
        else:
            bins.append(int(group))
    return bins

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Use: python %s <multirundir> <numruns> <all or range> < param_pairs.txt'\
                % __file__
        print "The argument of 'all' or 'range' means calculate the chi square of the whole distribution or use pre-defined ranges."
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    use_range = sys.argv[3].lower() == 'range'

    #bases = SCALING_FACTORS
    bases = (2.,)
    CENSUSES = (4, )
    lower_ranges = {
            1.5: "19:29",
            1.75: "14:21",
            2.0: "11:17",
            2.25: "10:15",
            2.5: "9:13"
    }
    upper_ranges = {
            1.5: "28:36",
            1.75: "21:26",
            2.0: "17:21",
            2.25: "15:18",
            2.5: "13:16"
    }

    print ';'.join(['Theta', 'm-rate', 'Regression fraction'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        for base in bases:
            print >>sys.stderr, theta, m
            if use_range:
                lower_range = bin_string_to_int_list(lower_ranges[base])
                upper_range = bin_string_to_int_list(upper_ranges[base])
                lower_range_frac, _ = partial_chisq_frac(
                        rundir + '/output',
                        numruns, lower_range, base)
                upper_range_frac, _ = partial_chisq_frac(
                        rundir + '/output',
                        numruns, upper_range, base)
                print "%s;%s;%f;%f;%f" % (theta, m, base, lower_range_frac,
                        upper_range_frac)
            else:
                results = "%s;%s;%f;" % (theta, m, base)
                for census in CENSUSES:
                    whole_dist_frac, obs_chisq = partial_chisq_frac(
                            rundir + '/output',
                            numruns, range(0, 75), base, rel_freq=True,
                            census=census)
                    results += "%f;%f;" % (obs_chisq, whole_dist_frac)
                print results
                    #print "%s;%s;%f;%f;%f" % (theta, m, base,
                            #obs_chisq, whole_dist_frac)
            sys.stdout.flush()

