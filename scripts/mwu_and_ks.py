'''
Mann-Whitney U and Kolgomorov-Smirnov tests on SAD and SBD

-- Run test on (all neutral runs catenated, observed)
-- Run test on (each neutral run, all neutral runs catenated)

"Then we look for the values of those statistics to be outside of 95% of the values we would get by comparing a given neutral run to the pooled neutral runs."
'''

import sys, os

from scipy import stats
import numpy as np

from data import readfile
from analysis import species_biomass

def run_test(fn, pooled, observed, neutral_runs):
    stat_obs = fn(pooled, observed)[0]

    stats_neutral = [fn(pooled, run)[0] for run in neutral_runs]

    stats_neutral = np.array(stats_neutral)

    gt_obs = len(stats_neutral[stats_neutral > stat_obs])
    lt_obs = len(stats_neutral[stats_neutral < stat_obs])

    num_sp = float(len(neutral_runs))

    return stat_obs, gt_obs / num_sp, lt_obs / num_sp

def mwu_and_ks(runs, observed):
    pooled = np.concatenate(runs)

    mwu = run_test(stats.mannwhitneyu, pooled, observed, runs)
    ks = run_test(stats.ks_2samp, pooled, observed, runs)

    return mwu, ks

def get_column(arrays, col_name):
    return [a[col_name] for a in arrays]

def mwu_and_ks_files(abund_files, col_name, observed):
    runs = get_column(abund_files, col_name)
    return mwu_and_ks(runs, observed)

def mwu_and_ks_sad(abund_files, observed_bci):
    obs_abundances = np.array([len(observed_bci.xs(sp)) \
            for sp in observed_bci.index.unique()])
    sad = mwu_and_ks_files(abund_files, 'sp_count', obs_abundances)

    return sad

def mwu_and_ks_sbd(abund_files, observed_bci):
    obs_biomasses = np.array([((observed_bci.xs(sp)['DBH'] / 10.)\
            ** (8./3.)).sum() for sp in observed_bci.index.unique()])
    sbd = mwu_and_ks_files(abund_files, 'sp_diam_pow', obs_biomasses)

    return sbd

def calculate_mwu_and_ks(rundir, numruns):
    runs = readfile.abund_files(rundir, numruns)
    observed = readfile.observed_bci()

    sad = mwu_and_ks_sad(runs, observed)
    sbd = mwu_and_ks_sbd(runs, observed)

    return sad, sbd

def format_row(tup):
    mwu, ks = tup

    mwu_str = ",".join(str(x) for x in mwu)
    ks_str = ",".join(str(x) for x in ks)

    return mwu_str + "," + ks_str

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt' \
                % __file__
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])

    print "Theta,m,SAD MWU,SAD MWU GT,SAD MWU LT,SAD KS,SAD KS GT,SAD KS LT,SBD MWU,SBD MWU GT,SBD MWU LT,SBD KS,SBD KS GT,SBD KS LT"

    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m

        sad, sbd = calculate_mwu_and_ks(os.path.join(rundir, 'output'), numruns)
        sad_str = format_row(sad)
        sbd_str = format_row(sbd)
        print theta + "," + m + "," + sad_str + "," + sbd_str

