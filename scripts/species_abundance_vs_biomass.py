import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import defaultdict
import sys, os
import analysis
from data import readfile

def create_dict_from_lists(key_list, item_list):
    key_items = defaultdict(list)
    for key, item in zip(key_list, item_list):
        key_items[key].append(item)
    return key_items

def key_and_mean_of_itemlist(dict_of_lists):
    keys = []
    mean_of_itemlists = []
    for key in dict_of_lists:
        keys.append(key)
        mean_of_itemlists.append(np.mean(dict_of_lists[key]))
    return keys, mean_of_itemlists

def plot_abundance_vs_biomass(rundir, numruns, plot_filename):
    abund_files = readfile.abund_files(rundir, numruns)
    concat_files = pd.concat(abund_files)
    abundance_biomass_pairs = zip(concat_files['sp_count'],
            concat_files['sp_diam_pow'])
    plot_filename = os.path.splitext(plot_filename)

    # plot individual runs
    '''
    for i, run in enumerate(abund_files):
        # linear - individual run
        individual_linear_unbinned = \
                '_individual_linear_unbinned_%d'.join(plot_filename) % i
        abundance_biomass_individual_run = zip(run['sp_count'],
                run['sp_diam_pow'])
        analysis.plot.bin_and_plot_pairs(abundance_biomass_individual_run,
            individual_linear_unbinned, plot_fn=plt.plot,
            xlabel='Species Abundance', ylabel='Mean Species Biomass')
        # loglog - individual run
        individual_loglog_unbinned = \
                '_individual_loglog_unbinned_%d'.join(plot_filename) % i
        analysis.plot.bin_and_plot_pairs(abundance_biomass_individual_run,
            individual_loglog_unbinned, plot_fn=plt.loglog,
            xlabel='Species Abundance', ylabel='Mean Species Biomass')
    '''

    # plot all runs
    # linear axes
    linear_unbinned = ''.join(plot_filename)
    analysis.plot.bin_and_plot_pairs(abundance_biomass_pairs, linear_unbinned,
            plot_fn=plt.plot, xlabel='Species Abundance',
            ylabel='Mean Species Biomass')
    linear_linear10 = '_linear10_binned'.join(plot_filename)
    analysis.plot.bin_and_plot_pairs(abundance_biomass_pairs, linear_linear10,
            plot_fn=plt.plot, bin_fn=lambda x: round(x, -1),
            xlabel='Species Abundance', ylabel='Mean Species Biomass')

    # loglog axes
    loglog_unbinned = '_loglog'.join(plot_filename)
    analysis.plot.bin_and_plot_pairs(abundance_biomass_pairs, loglog_unbinned,
            plot_fn=plt.loglog, xlabel='Species Abundance',
            ylabel='Mean Species Biomass')
    loglog_linear10 = '_loglog_linear10_binned'.join(plot_filename)
    analysis.plot.bin_and_plot_pairs(abundance_biomass_pairs, loglog_linear10,
            plot_fn=plt.loglog, bin_fn=lambda x: round(x, -1),
            xlabel='Species Abundance', ylabel='Mean Species Biomass')

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt'\
                % __file__
        exit()
    multirundir = os.path.normpath(sys.argv[1])
    numruns = int(sys.argv[2])

    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m
        savename = '../output/' + 'abundance-vs-biomass_' + '_'.join(rundir.split('/')[-3:]) + '.png'
        plot_abundance_vs_biomass(rundir + '/output', numruns, savename)
