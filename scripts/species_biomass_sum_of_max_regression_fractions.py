import sys
import numpy as np
from pandas.io import parsers
from data import readfile, SCALING_FACTORS, STATIC_FILES
from analysis import species_biomass
import os.path
from scipy import stats
from collections import namedtuple, defaultdict
import matplotlib.pyplot as plt
import operator


Range = namedtuple('Range', ('start', 'mid', 'end'))

def two_point_slope(bins, index1, index2):
    assert(index2 != index1)
    v1 = 0
    v2 = 0
    if len(bins) - 1 >= index2:
        v1 = bins[index1]
        v2 = bins[index2]
    elif len(bins) - 1 >= index1:
        v1 = bins[index1]
    return (v2 - v1) / (index2 - index1)

def regression_slope(bins, start_i, end_i):
    if (len(bins) - 1) - start_i < 3:
        return two_point_slope(bins, start_i, len(bins) - 1)
    return stats.linregress(np.arange(end_i - start_i), bins[start_i:end_i])[0]

def slope(dist, start_i, end_i):
    if end_i - start_i > 1:
        slope = regression_slope(dist, start_i, end_i + 1)
    else:
        slope = two_point_slope(dist, start_i, end_i)
    return slope

def slope_difference(dist, range_bounds):
    slope_start_mid = slope(dist, range_bounds.start, range_bounds.mid)
    slope_mid_end = slope(dist, range_bounds.mid, range_bounds.end)
    return slope_mid_end - slope_start_mid

def max_slope_differences_for_each_bin(dist, min_window_size=1,
        max_window_size=8):
    '''
    for bin in dist:
        for left in range():
            for right in range():
                slope_diff = slope_difference()
                if slope_diff > dist_maximums[bin]:
                    dist_maximums[bin] = slope_diff
    return dist_maximums
    '''
    #dist_maximums = defaultdict(tuple)
    dist_maximums = {}
    for i in range(len(dist)):
        if i - min_window_size < 0:
            continue
        elif i + min_window_size > len(dist):
            continue

        for left_offset in range(min_window_size, max_window_size + 1):
            if i - left_offset < 0:
                break
            for right_offset in range(min_window_size, max_window_size + 1):
                if i + right_offset > len(dist) - 1:
                    break
                window = Range(i - left_offset, i, i + right_offset)
                window_diff = slope_difference(dist, window)
                #FIX: now this is wrong, need another way to compare
                try:
                    if window_diff > dist_maximums[i][0]:
                        dist_maximums[i] = (window_diff, window)
                except KeyError, e:
                    dist_maximums[i] = (window_diff, window)
    return dist_maximums

def sum_of_max_two_slope_differences(max_slope_differences):
    #sorted_keys_by_value = [key for key, value in
            #sorted(max_slope_differences.iteritems(),
            #key=operator.itemgetter(1), reverse=True)]
    sorted_values = sorted(max_slope_differences.values(), reverse=True)

    maximums = [sorted_values[0][0]]
    prev_range = sorted_values[0][1]
    i = 1
    while prev_range.start <= sorted_values[i][1].mid <= prev_range.end:
        prev_range = sorted_values[i][1]
        i += 1
    maximums.append(sorted_values[i][0])
    return sum(maximums), [sorted_values[0][1].mid, sorted_values[i][1].mid]

    '''
    print sorted_values,'\n'
    print [max_slope_differences[i] for i in sorted_keys_by_value]
    max_key = sorted_keys_by_value[0]
    max_range = max_slope_differences[max_key][1]
    next_index = 1
    next_key = sorted_keys_by_value[next_index]
    while max_range.start <= next_key <= max_range.end:
        try:
            next_key = sorted_keys_by_value[next_index]
        except KeyError:
            raise Exception('No second maximum outside of first maximum\'s range')
    sum_of_maximums = sum(max_slope_differences[bin][0]
            for bin in keys_with_max_values)
    return sum_of_maximums, keys_with_max_values
    '''


def calculate_sum(dist, min_window_size=1, max_window_size=12):
    maximums = max_slope_differences_for_each_bin(dist, min_window_size,
            max_window_size)
    return sum_of_max_two_slope_differences(maximums)


def slope_fractions(obs_dist, each_run_dists, minimum_window=1):
    #print >>sys.stderr, 'obs', obs_diff
    #obs_slope_difference = slope_difference(obs_dist, lower_range) \
            #+ slope_difference(obs_dist, upper_range)

    # TODO: base 1.5 min window size
    try:
        obs_slope_difference, obs_bins_with_max_diffs = calculate_sum(obs_dist,
            min_window_size=minimum_window)
    except Exception, e:
        print 'Error', e
        return None
    #print 'obs slope diff', obs_slope_difference

    slope_diffs = []
    bins_with_max_diffs_counter = np.zeros(60)
    for i, run_dist in enumerate(each_run_dists):
        try:
            sum_stat, bins_with_max_diffs = calculate_sum(run_dist,
                min_window_size=minimum_window)
        except:
            return None
        for bin in bins_with_max_diffs:
            bins_with_max_diffs_counter[bin] += 1
        slope_diffs.append(sum_stat)
        #print run_dist, sum_stat

    print 'slope diffs', slope_diffs
    print 'bin counter', bins_with_max_diffs_counter

    slope_diffs = np.array(slope_diffs)
    #print >>sys.stderr, 'slopes', slope_diffs.min(), slope_diffs.mean(), sorted(slope_diffs)[len(slope_diffs)/2], slope_diffs.max()

    run_diffs_gt_obs_diff = len(slope_diffs[slope_diffs > obs_slope_difference])

    return run_diffs_gt_obs_diff / float(len(slope_diffs))


def compare_slopes(rundir, numruns, base, fname_start_index=0):
    each_run_dists, each_run_dists_pow = species_biomass.\
            calculate_each_run_distribution(rundir, numruns, base=base,
            fname_start_index=fname_start_index)

    log_dist_pow = os.path.join(STATIC_FILES, 'observed_data',
            'observed_species_biomass_distributions',
            'base_%0.3f.txt' % base)
    observed_dist_pow = parsers.read_table(log_dist_pow,
            names=['bins'], dtype=np.float)['bins']
    min_window = 1
    if base == 1.5:
        min_window = 2
    return slope_fractions(observed_dist_pow, each_run_dists_pow,
            minimum_window=min_window)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt'\
                % __file__
        print '(Use zero-based indexes)'
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    #bases = SCALING_FACTORS
    bases = (2.,)

    print ';'.join(['Theta', 'm-rate', 'Regression fraction'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        for base in bases:
            print >>sys.stderr, theta, m
            sum_range_regressions = compare_slopes(rundir + '/output', numruns,
                    base)
            if sum_range_regressions:
                print "%s;%s;%f;%f" % (theta, m, base, sum_range_regressions)
                sys.stdout.flush()
