import sys
import numpy as np
from data import readfile
from analysis import plot

E = 2.718281

def remove_zeros_from_two_arrays(array1, array2):
    sb_not_0 = array1 != 0
    array1 = array1[sb_not_0]
    array2 = array2[sb_not_0]
    li_not_0 = array2 != 0
    array1 = array1[li_not_0]
    array2 = array2[li_not_0]
    return array1, array2

def normalize_length(li, length):
    li = li[:length]
    if len(li) < length:
        li = np.append(li, [0]*(length - len(li)))
    return li

class SBDLargestIndividualFigure(plot.Figure):
    def __init__(self, rundir, numruns, *args, **kw):
        super(SBDLargestIndividualFigure, self).__init__(*args, **kw)
        self.indivs = readfile.individual_files(rundir, numruns)

    def build_figure(self, mean=False, plot_runs=False):
        biomasses = []
        largest = []
        for i, indiv in enumerate(self.indivs):
            species = indiv.index.unique()
            species_biomasses = np.array([sum(indiv.xs(uniq)['diam_pow']) \
                    for uniq in species])
            largest_individuals = np.array([max(indiv.xs(uniq)['diam_pow']) \
                    for uniq in species])
            if not mean:
                species_biomasses, largest_individuals = \
                        remove_zeros_from_two_arrays(
                        species_biomasses, largest_individuals)
                self.ax.loglog(largest_individuals, species_biomasses, 'g.',
                        basex=E, basey=E)
                if plot_runs:
                    self.savefig("something.png")
                    self.clf()
            else:
                biomasses.append(np.mean(species_biomasses))
                largest.append(np.mean(largest_individuals))

        if mean:
            # something weird, dunno lol
            self.ax.loglog(largest, biomasses, 'g.', basex=E, basey=E)
        self.set_ylabel("Species biomass (natural log)")
        self.set_xlabel("Largest individual biomass (natural log)")


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Use: python %s <rundir> <numruns> <mean? true or false>' % __file__
        exit()
    rundir = sys.argv[1]
    numruns = int(sys.argv[2])
    use_mean = False
    if sys.argv[3].lower() == "true":
        use_mean = True

    figure = SBDLargestIndividualFigure(rundir, numruns)
    figure.build_figure(mean=use_mean, plot_runs=False)
    figure.savefig('sbd_largest_individual.png')
