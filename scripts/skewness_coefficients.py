import numpy as np
from data import readfile
import sys, os
from math import floor, log, isnan
from collections import defaultdict
from functools import partial
import matplotlib.pyplot as plt
from scipy import stats
from pandas.io import parsers

def skewness_coefficient(a):
    '''
    Fisher-Pearson standardized moment coefficient
    (g values)
    '''
    x_bar = a.mean()
    s = a.std(ddof=1)
    n = len(a)
    sum_term = sum((x_i -x_bar)**3 for x_i in a)
    return n * sum_term / ((n - 1) * (n - 2) * s**3)

def individuals_array(df):
    nonzero_size_classes = df['num_individuals'] != 0
    nonzero_num_individuals = np.array(
            df['num_individuals'])[nonzero_size_classes]
    diams = np.array(df['diam'])[nonzero_size_classes]
    diams /= nonzero_num_individuals
    return np.concatenate([[log(d, 10)]*n
        for d, n in zip(diams, nonzero_num_individuals)])

def bin_and_plot(individuals_gval_pairs, graph_filename, bin_fn=None,
        plot_fn=None, xlim=None, ylim=None, savefig=True, clf=True, style='b.'):
    if not bin_fn:
        do_nothing = lambda x: x
        bin_fn = do_nothing
    if not plot_fn:
        plot_fn = plt.plot
    #individuals_gval_pairs.sort()
    individuals = defaultdict(list)
    for pair in individuals_gval_pairs:
        individuals[bin_fn(pair[0])].append(pair[1])

    num_individuals = []
    g_values = []
    for key in sorted(individuals):
        num_individuals.append(key)
        g_values.append(np.mean(individuals[key]))

    plot_fn(num_individuals, g_values, style)
    plt.xlabel('Number of Individuals')
    plt.ylabel('Mean g value')
    if xlim:
        plt.xlim(xlim)
    if ylim:
        plt.ylim(ylim)
    if savefig:
        plt.savefig(graph_filename)
    if clf:
        plt.clf()

def run_regression(pairs):
    num_individuals = [pair[0] for pair in pairs]
    g_values = [pair[1] for pair in pairs]
    return stats.linregress(num_individuals, g_values)

BIN_FACTOR = 0.5
def g_value_bin(g_val):
    return round(g_val / BIN_FACTOR) * BIN_FACTOR

def dict_to_array(d, min_key=None, max_key=None):
    '''
    Takes dict with integer keys, int/float values
    Returns array of floats representing the contents of d
    '''
    if not min_key:
        offset = min(d)
    else:
        offset = min_key
    if not max_key:
        max_key = max(d)
    offset, max_key = offset / BIN_FACTOR, max_key / BIN_FACTOR
    array = np.zeros(max_key + 1 - offset)
    for key in d:
        array[key / BIN_FACTOR - offset] = d[key]
    return array

def calculate_g_dist(g_vals):
    g_bins = defaultdict(int)
    for g in g_vals:
        g_bins[g_value_bin(g)] += 1
    return g_bins

def dict_chisquare(observed, expected):
    min_key = min(min(observed), min(expected))
    max_key = max(max(observed), max(expected))
    obs_dist = dict_to_array(observed, min_key=min_key, max_key=max_key)
    exp_dist = dict_to_array(expected, min_key=min_key, max_key=max_key)
    nonzero_exp = exp_dist != 0
    return stats.chisquare(obs_dist[nonzero_exp], exp_dist[nonzero_exp])

def calculate_skewness_values(rundir, numruns, obs_g_values, fname_start_index=0):
    cutoff = 1.0

    obs_g_bins = calculate_g_dist(obs_g_values)
    obs_g_below_cutoff = len(obs_g_values[obs_g_values <= cutoff])

    indivs = readfile.individual_files(rundir, numruns, fname_start_index)

    g_distribution = defaultdict(int)
    species_num_individuals_g_pairs = []
    run_g_vals = defaultdict(list)

    run_g_vals_below_cutoff = defaultdict(int) ###############
    run_species_num_individuals_g_pairs = defaultdict(list)
    for run_index, fname in readfile.full_filename_generator(rundir, numruns,
            'Individuals_', '_1.txt', start_index=fname_start_index):
        indiv = readfile.individual_file(fname)
        for species_id in indiv.index.unique():
            diams = individuals_array(indiv.xs(species_id))
            #if len(diams) > 2:
            if len(diams) >= 5:
                g = skewness_coefficient(diams)
                if not isnan(g):
                    index = g_value_bin(g)
                    g_distribution[index] += 1
                    species_pair = (len(diams), g)
                    species_num_individuals_g_pairs.append(species_pair)
                    run_species_num_individuals_g_pairs[run_index].append(
                            species_pair)
                    run_g_vals[run_index].append(g)
                    if g <= cutoff:
                        run_g_vals_below_cutoff[run_index] += 1

    #print 'Bin', 'Mean number of species'
    mean_g_vals_below_cutoff = 0.0
    for bin in sorted(g_distribution):
        #print bin, g_distribution[bin] / float(len(indivs))
        g_distribution[bin] /= float(len(indivs))
        if bin <= cutoff:
            mean_g_vals_below_cutoff += g_distribution[bin]

    chisq_obs_mean = dict_chisquare(obs_g_bins, g_distribution)

    chisq_run_obs = []
    for run in run_g_vals:
        run_g_dist = calculate_g_dist(run_g_vals[run])
        #chisq_run_obs.append(dict_chisquare(run_g_dist, obs_g_bins))
        chisq_run_obs.append(dict_chisquare(run_g_dist, g_distribution))

    runs_below_cutoff_above_mean = 0
    for run in run_g_vals_below_cutoff:
        #print run_g_vals_below_cutoff[run], mean_g_vals_below_cutoff,\
                #obs_g_below_cutoff
        #print run_g_vals_below_cutoff[run] - mean_g_vals_below_cutoff,\
            #obs_g_below_cutoff - mean_g_vals_below_cutoff
        if run_g_vals_below_cutoff[run] - mean_g_vals_below_cutoff \
                > obs_g_below_cutoff - mean_g_vals_below_cutoff:
            runs_below_cutoff_above_mean += 1

    chisq_run_obs = np.array(chisq_run_obs[0])
    fraction_gt_mean = len(chisq_run_obs[chisq_run_obs > chisq_obs_mean[0]]) \
            / float(len(chisq_run_obs))
    fraction_below_cutoff_gt_mean = runs_below_cutoff_above_mean \
            / float(len(chisq_run_obs))

    # plotting
    obs_num_individuals = np.array(sorted(obs_g_bins))
    obs_gs = np.array([obs_g_bins[key] for key in obs_num_individuals])

    mean_num_individuals = np.array(sorted(g_distribution))
    mean_gs = np.array([g_distribution[key] for key in mean_num_individuals])
    # actually plot g values vs number of species, not number of individuals
    # bad naming
    '''
    plt.plot(obs_num_individuals, obs_gs, 'g-')
    plt.plot(mean_num_individuals, mean_gs, 'b-')
    plt.xlim([-2, 2])
    plt.ylim([0, 100])
    plt.savefig('../output/g_vals/g_val_mean_vs_observed.png')
    plt.clf()


    for run in run_g_vals:
        run_g_dist = calculate_g_dist(run_g_vals[run])
        run_num_individuals = np.array(sorted(run_g_dist))
        run_gs = np.array([run_g_dist[key] for key in run_num_individuals])
        plt.plot(obs_num_individuals, obs_gs, 'g-')
        plt.plot(run_num_individuals, run_gs, 'b-')
        plt.xlim([-2, 2])
        plt.ylim([0, 100])
        plt.savefig('../output/g_vals/g_val_individual_run_%d.png' % run)
        plt.clf()
    '''

    return chisq_obs_mean, np.mean(chisq_run_obs), fraction_gt_mean, \
            fraction_below_cutoff_gt_mean


    '''
    # plots of abundance vs g values with different axis scales
    bin_and_plot(species_num_individuals_g_pairs,
            '../output/g_val_individuals.png')
    bin_and_plot(species_num_individuals_g_pairs,
            '../output/g_val_individuals_log10_binned.png',
            bin_fn=partial(log, 10))
    bin_and_plot(species_num_individuals_g_pairs,
            '../output/g_val_individuals_semilogx.png',
            plot_fn=plt.semilogx)
    bin_and_plot(species_num_individuals_g_pairs,
            '../output/g_val_individuals_linear10_binned.png',
            bin_fn=lambda x: round(x, -1))
    '''

    '''
    # regression of abundance vs g values
    slopes = []
    for run in run_species_num_individuals_g_pairs:
        pairs = run_species_num_individuals_g_pairs[run]
        #import pdb; pdb.set_trace()
        slopes.append(run_regression(pairs)[0])
        bin_and_plot(pairs, '../output/g_vals/g_val_individual_run_%d.png' % run,
                ylim=[-4, 4])
        bin_and_plot(pairs,
                '../output/g_vals/g_val_individual_run__semilog_%d.png' % run,
                plot_fn=plt.semilogx, ylim=[-4, 4])
    print 'Mean slope:', np.mean(slopes)
    '''

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Use: python %s <multirundir> <numruns> <observed g values file> < param_pairs.txt'\
                % __file__
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])

    obs_g_values = parsers.read_table(sys.argv[3], sep='\t')['g_value']

    print ';'.join(['Theta', 'm-rate', 'Chi-square mean neutral', 'Mean chi-square across runs', 'Fraction of runs above mean neutral'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m
        results = calculate_skewness_values(os.path.join(rundir, 'output'),
            numruns, obs_g_values)
        chisq_obs_mean, mean_run_chisq, chisq_fraction, cutoff_fraction = results
        print '%s;%s;%f;%f;%f;%f' \
                % (theta, m, chisq_obs_mean[0], mean_run_chisq,
                        chisq_fraction, cutoff_fraction)
        sys.stdout.flush()



