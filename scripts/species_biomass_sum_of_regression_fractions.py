import sys
import numpy as np
from pandas.io import parsers
from data import readfile, STATIC_FILES, SCALING_FACTORS
from analysis import species_biomass
import os.path
from scipy import stats
from collections import namedtuple

import matplotlib.pyplot as plt

def two_point_slope(bins, index1, index2):
    assert(index2 != index1)
    v1 = 0
    v2 = 0
    if len(bins) - 1 >= index2:
        v1 = bins[index1]
        v2 = bins[index2]
    elif len(bins) - 1 >= index1:
        v1 = bins[index1]
    return (v2 - v1) / (index2 - index1)

def regression_slope(bins, start_i, end_i):
    if (len(bins) - 1) - start_i < 3:
        return two_point_slope(bins, start_i, len(bins) - 1)
    return stats.linregress(np.arange(end_i - start_i), bins[start_i:end_i])[0]

def slope(dist, start_i, end_i):
    if end_i - start_i > 1:
        slope = regression_slope(dist, start_i, end_i + 1)
    else:
        slope = two_point_slope(dist, start_i, end_i)
    return slope

def slope_difference(dist, range_bounds):
    slope_start_mid = slope(dist, range_bounds.start, range_bounds.mid)
    slope_mid_end = slope(dist, range_bounds.mid, range_bounds.end)
    return slope_mid_end - slope_start_mid

def slope_fractions(obs_dist, each_run_dists, lower_range, upper_range):
    #print >>sys.stderr, 'obs', obs_diff
    obs_slope_difference = slope_difference(obs_dist, lower_range) \
            + slope_difference(obs_dist, upper_range)

    slope_diffs = []
    for i, run_dist in enumerate(each_run_dists):
        #print run_dist
        #plt.plot(range(len(obs_dist)), obs_dist, 'b-')
        #plt.plot(range(len(run_dist)), run_dist, 'g-')
        #plt.savefig('../output/regression_fractions_2.50/%d.png' %i)
        #plt.clf()

        lower_difference = slope_difference(run_dist, lower_range)
        upper_difference = slope_difference(run_dist, upper_range)
        slope_diffs.append(lower_difference + upper_difference)

    slope_diffs = np.array(slope_diffs)
    #print >>sys.stderr, 'slopes', slope_diffs.min(), slope_diffs.mean(), sorted(slope_diffs)[len(slope_diffs)/2], slope_diffs.max()

    run_diffs_gt_obs_diff = len(slope_diffs[slope_diffs > obs_slope_difference])

    return run_diffs_gt_obs_diff / float(len(slope_diffs))


def compare_slopes(rundir, numruns, base, lower_range, upper_range,
        fname_start_index=0):
    each_run_dists, each_run_dists_pow = species_biomass.\
            calculate_each_run_distribution(rundir, numruns, base=base,
            fname_start_index=fname_start_index)

    log_dist_pow = os.path.join(STATIC_FILES, 'observed_data',
            'observed_species_biomass_distributions',
            'base_%0.3f.txt' % base)
    observed_dist_pow = parsers.read_table(log_dist_pow,
            names=['bins'], dtype=np.float)['bins']
    return slope_fractions(observed_dist_pow, each_run_dists_pow,
            lower_range, upper_range)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt'\
                % __file__
        print '(Use zero-based indexes)'
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    Range = namedtuple('Range', ('start', 'mid', 'end'))
    #bases = (1.5, 1.75, 2., 2.25, 2.5)
    bases = SCALING_FACTORS
    lower_ranges = {
            1.5: Range(19, 21, 29),
            1.75: Range(14, 15, 21),
            2.0: Range(11, 12, 17),
            2.25: Range(10, 11, 15),
            2.5: Range(9, 10, 13)
    }
    upper_ranges = {
            1.5: Range(28, 34, 36),
            1.75: Range(21, 25, 26),
            2.0: Range(17, 20, 21),
            2.25: Range(15, 17, 18),
            2.5: Range(13, 15, 16)
    }

    print ';'.join(['Theta', 'm-rate', 'Regression fraction'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        for base in bases:
            print >>sys.stderr, theta, m
            sum_range_regressions = compare_slopes(rundir + '/output', numruns,
                    base, lower_ranges[base], upper_ranges[base])
            print "%s;%s;%f;%f" % (theta, m, base, sum_range_regressions)
            sys.stdout.flush()
