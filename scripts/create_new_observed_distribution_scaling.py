'''
Prints the species biomass log distribution for arbitrary scaling (s) values
'''
import sys
import os
import pandas as pd
import numpy as np
import data
from analysis import species_biomass

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "Usage: %s <parent folder of census folders>" \
                % __file__
        exit()
    output_folder = os.path.normpath(sys.argv[1])


    #obs_data = pd.io.parsers.read_table(obs_file, sep='\t', index_col=1)
    for census in data.CENSUSES:
        census_folder = os.path.join(output_folder, "census%d" % census)
        for base in data.SCALING_FACTORS:
            sp_biomass_dist = species_biomass.observed_log_dist(base,
                    census=census)
            output_file = os.path.join(census_folder, "base_%0.3f.txt" % base)
            if not os.path.exists(output_file):
                print >>sys.stderr, output_file
                with open(output_file, 'w') as f:
                    print >>f, "\n".join(str(v) for v in sp_biomass_dist)

