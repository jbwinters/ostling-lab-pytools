import sys
import os
import shutil
from data import readfile

def filename_generator(fileprefix, filepost, numruns,startindex=0):
    for i in range(startindex,numruns+startindex):
        yield ("{0}{1}{2}".format(fileprefix,str(i),filepost),i)

def cp_missing_files(directory, prefix, filepost, numruns):
    exist = set()
    dont_exist = set()
    for fname, i in filename_generator(prefix, filepost, numruns):
        fname = directory + "/" + fname
        if os.path.exists(fname):
            exist.add(fname)
        else:
            dont_exist.add(fname)
    if len(exist) == 0:
        print "Files do not exist.", directory
        exit(1)
    if len(dont_exist) > 100:
        print "Not enough existing files:", directory, prefix
        exit(1)
    for nonfile in dont_exist:
        cp_file = exist.pop()
        shutil.copy(cp_file, nonfile)

def main(directory, numruns):
    cp_missing_files(directory, 'Abund_', '_1.txt', numruns)
    cp_missing_files(directory, 'SD_', '_1.txt', numruns)
    cp_missing_files(directory, 'logSAD_', '_1.txt', numruns)
    cp_missing_files(directory, 'Individuals_', '_1.txt', numruns)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print "Usage: python cp_missing.py <multirundir> <number of runs>"
        exit(0)
    directory = os.path.normpath(sys.argv[1])
    for rundir, theta, m in readfile.rundir_gen(directory):
        print >>sys.stderr, theta, m
        rundir = os.path.join(rundir, 'output')
        num_runs = int(sys.argv[2])
        main(rundir, num_runs)
