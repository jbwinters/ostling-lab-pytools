import numpy as np
import sys, os
from data import readfile

def species_richness_frac(rundir, numruns):
    obs_sp_rich = sum(readfile.logSAD_bci())

    sp_richnesses = [len(abund)
            for abund in readfile.abund_files(rundir, numruns)]
    sp_richnesses = np.array(sp_richnesses)

    return len(sp_richnesses[sp_richnesses > obs_sp_rich]) \
            / float(len(sp_richnesses))

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Use: python %s <multirundir> <numruns> < param_pairs.txt'\
                % __file__
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])

    print ';'.join(['Theta', 'm-rate', 'Fraction of runs above observed'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m
        frac_sp_rich = species_richness_frac(os.path.join(rundir, 'output'),
            numruns)
        print '%s;%s;%f' \
                % (theta, m, frac_sp_rich)
        sys.stdout.flush()



