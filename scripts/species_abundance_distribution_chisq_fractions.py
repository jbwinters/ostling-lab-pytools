import sys
import numpy as np
from pandas.io import parsers
from data import readfile
from analysis import species_abundance
import os.path
from scipy import stats

def chisq_fraction_abundance_distributions(rundir, numruns, obs_dist,
        fname_start_index=0, rel_freq=False):
    # calculate abundance distribution for each run
    # calculate chisq of each run
    if rel_freq:
        obs_dist = obs_dist / sum(obs_dist)
    sads = []
    run_chisqs = []
    for logSAD_file in readfile.logSAD_files(rundir, numruns):
        sad = logSAD_file['bins']
        if rel_freq:
            sad /= sum(logSAD_file['bins'])
        sads.append(sad)

    # calculate mean, chisq of mean
    #mean_sad = species_abundance.mean_species_abundance_distribution(sads)
    mean_sad = sum(sads) / float(len(sads))
    mean_neutral_chisq = species_abundance.distribution_chisq(obs_dist,
            mean_sad)[0]

    chisq_arr = np.array([species_abundance.distribution_chisq(sad, mean_sad)[0]
                    for sad in sads])

    # calculate fraction over mean
    return mean_neutral_chisq, \
            len(chisq_arr[chisq_arr > mean_neutral_chisq]) / float(len(chisq_arr))

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Use: python %s <multirundir> <numruns> <observed abundance file> < param_pairs.txt'\
                % __file__
        exit()
    multirundir = sys.argv[1]
    numruns = int(sys.argv[2])
    obs_file = sys.argv[3]
    obs_dist = parsers.read_table(obs_file, names=['bins'],
            dtype=np.float)['bins']

    print ';'.join(['Theta', 'm-rate', 'Fraction of chi-squares above mean neutral'])
    for rundir, theta, m in readfile.rundir_gen(multirundir):
        print >>sys.stderr, theta, m
        mn_chisq, frac_chisqs = chisq_fraction_abundance_distributions(rundir + '/output',
                numruns, obs_dist, rel_freq=True)
        print "%s;%s;%f;%f" % (theta, m, mn_chisq, frac_chisqs)
        sys.stdout.flush()
 
