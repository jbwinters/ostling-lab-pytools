import os
import sys
from analysis import size_distribution

MULTIRUN3 = os.path.normpath(os.environ['MULTIRUN3'])
HYBRID_WEIBULL = os.path.normpath(os.environ['HYBRID_OBSERVED_WEIBULL'])

if len(sys.argv) < 2:
    print 'Usage: python %s <output file> <bin size>' % __file__
    exit()
if len(sys.argv) > 3:
    print 'Usage: python %s <output file> <bin size>' % __file__
    exit()

outfile = ""
if len(sys.argv) == 3:
    outfile = sys.argv[1]

rundir_lc = os.path.join(MULTIRUN3, 'theta_49-m_0.13', 'output')
rundir_hyb = os.path.join(HYBRID_WEIBULL, 'output2', 'theta_44-m_0.21', 'output')
numruns = 1000

if outfile:
    binsize = float(sys.argv[2]) # bin size of 5 = 10cm
else:
    binsize = float(sys.argv[1])
remove_last_bin = False

sd = size_distribution.\
        calculate_mean_size_distribution(rundir_lc, numruns,
            BINSIZE=binsize, remove_last_bin=remove_last_bin)
sd_hyb = size_distribution.\
        calculate_mean_size_distribution(rundir_hyb, numruns,
            BINSIZE=binsize, remove_last_bin=remove_last_bin)
if outfile:
    with open(outfile, 'w') as f:
        for a, b in zip(sd, sd_hyb):
            print >>f, a, b
else:
    for a, b in zip(sd, sd_hyb):
        print a, b
