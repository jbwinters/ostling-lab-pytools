from math import ceil, log
from scipy import stats
import numpy as np

def index_of_bin(elt, base):
    return int(ceil(log(elt, base)))

def log_scaled_bin_distribution(li, base=2.0, dist_len=None):
    if not dist_len:
        dist_len = 52
    index_of_max_elt = index_of_bin(max(li), base)
    if dist_len < index_of_max_elt:
        dist_len = index_of_max_elt + 1
    dist=[0]*dist_len
    for elt in li:
        if elt <= 0:
            continue
        bin_index = index_of_bin(elt, base)
        if bin_index < 0:
            bin_index = 0
        elif bin_index > dist_len - 1:
            bin_index = dist_len
        dist[bin_index] += 1
    return np.array(dist)

def distribution(species_sum_diams, base=2):
    # not useful, don't use
    species_diams = np.concatenate([diams for diams in species_sum_diams])
    return log_scaled_bin_distribution(species_diams, base) \
            / float(len(species_sum_diams))

def distribution_chisq(species_diam_dist, observed_distribution):
    nonzeros = species_diam_dist != 0
    return stats.chisquare(species_diam_dist[nonzeros],
            observed_distribution[diams])

def chisq(species_diam_dist, observed_distribution):
    # TODO: ???? probably incorrect, swap obs, exp
    nonzeros = species_diam_dist != 0
    return stats.chisq(species_diam_dist[nonzeros],
            observed_distribution[nonzeros])

def chisq_bin_contributions(species_diam_dist, observed_distribution):
    nonzeros = species_diam_dist != 0
    chisqs = []
    for obs, exp in zip(observed_distribution[nonzeros],
            species_diam_dist[nonzeros]):
        chisqs.append(stats.chisquare(np.array([obs]), np.array([exp]))[0])
    return chisqs

################################
# Functions involving file i/o #
################################
from data import readfile
from pandas.io import parsers
import os

STATIC_FILES = os.path.normpath(os.environ['STATIC_FILES'])

#TODO: refactor these

def calculate_mean_distribution(rundir, numruns, fname_start_index=0):
    abunds = readfile.abund_files(rundir, numruns, start_index=fname_start_index)
    sp_diams = [abund['sp_diam'] for abund in abunds]
    sp_diams_pow = [abund['sp_diam_pow'] for abund in abunds]
    return distribution(sp_diams), distribution(sp_diams_pow)

def calculate_each_run_distribution(rundir, numruns, base=2.0,
        fname_start_index=0):
    abunds = readfile.abund_files(rundir, numruns, start_index=fname_start_index)
    distributions = []
    distributions_pow = []
    for abund in abunds:
        distributions.append(distribution([abund['sp_diam']], base))
        distributions_pow.append(distribution([abund['sp_diam_pow']], base))
    return distributions, distributions_pow

def calculate_chisq_bin_contributions_helper(sp_diams, **kwargs):
    diam_dist = distribution(sp_diams)
    if 'observed_dist' in kwargs:
        observed_dist = kwargs['observed_dist']
    elif 'observed_dist_filename' in kwargs:
        observed_dist = parsers.read_table(kwargs['observed_dist_filename'],
                names=['bins'], dtype=np.float)['bins']
    return chisq_bin_contributions(diam_dist, observed_dist)

def calculate_chisq_bin_contributions(rundir, numruns, fname_start_index=0):
    abunds = readfile.abund_files(rundir, numruns, start_index=fname_start_index)
    sp_diams = [abund['sp_diam'] for abund in abunds]
    log2_dist = os.path.join(STATIC_FILES,
            'observed_data/log2_species_biomass_distribution.txt')
    bin_chisqs = calculate_chisq_bin_contributions_helper(sp_diams,
            observed_dist_filename=log2_dist)

    sp_diams_pow = [abund['sp_diam_pow'] for abund in abunds]
    log2_dist_pow = os.path.join(STATIC_FILES,
            'observed_data/log2_species_biomass_distribution_pow8-3.txt')
    bin_chisqs_pow = calculate_chisq_bin_contributions_helper(
            sp_diams_pow, observed_dist_filename=log2_dist_pow)

    return (bin_chisqs, bin_chisqs_pow)

def calculate_each_run_chisq_bins_vs_all_runs(
        rundir, numruns, fname_start_index=0):
    abunds = readfile.abund_files(rundir, numruns, start_index=fname_start_index)
    #
    sp_diams = [abund['sp_diam'] for abund in abunds]
    sp_diams_pow = [abund['sp_diam_pow'] for abund in abunds]
    diam_dist = distribution(sp_diams)
    diam_dist_pow = distribution(sp_diams_pow)
    #
    bin_chisqs = []
    bin_chisqs_pow = []
    for abund in abunds:
        sp_diams = [abund['sp_diam']]
        sp_diams_pow = [abund['sp_diam_pow']]
        bin_chisqs.append(
                calculate_chisq_bin_contributions_helper(sp_diams,
                    observed_dist=diam_dist))
        bin_chisqs_pow.append(
                calculate_chisq_bin_contributions_helper(sp_diams_pow,
                    observed_dist=diam_dist_pow))
    return (bin_chisqs, bin_chisqs_pow)

def observed_log_dist(scaling_factor=2.0, census=4):
    array = readfile.observed_bci(census=census)
    species_biomasses = np.array([((array.xs(sp)['DBH'] / 10.) ** (8./3.)).sum()
        for sp in array.index.unique()])
    sp_biomass_dist = log_scaled_bin_distribution(species_biomasses,
            base=scaling_factor)
    return sp_biomass_dist

