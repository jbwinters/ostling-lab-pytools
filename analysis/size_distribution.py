import sys
from math import floor

import numpy as np

from data import readfile

def size_distribution(individuals, num_size_classes=125,
        BINSIZE=None, remove_last_bin=True):
    '''
    sizes is a two-column array - DBH, num_individuals
    '''
    if not BINSIZE:
        BINSIZE = 5.0
    else:
        BINSIZE = float(BINSIZE)
    size_dist = np.zeros(int(floor(num_size_classes / BINSIZE)))
    sizes = individuals['size_class']
    num_individuals = individuals['num_individuals']
    for size, num_individuals in zip(sizes, num_individuals):
        sizeclass=int(floor(size/BINSIZE))
        if remove_last_bin \
                and sizeclass == (int(floor(num_size_classes / BINSIZE)) - 1):
            continue
        size_dist[sizeclass] += num_individuals
    return size_dist

def calculate_mean_size_distribution(rundir, numruns,
        four_cols=False, BINSIZE=None, remove_last_bin=True):
    dists = []
    i = 0
    import time
    start_time = time.time()
    for _, fname in readfile.full_filename_generator(rundir, numruns,
            'Individuals_', '_1.txt', start_index=0):
        if i % 10 == 0:
            end_time = time.time()
            print i, end_time - start_time
            start_time = time.time()
        i += 1
        try:
            indiv = readfile.individual_file(fname, four_cols=four_cols)
        except Exception, e:
            print 'Error reading:', fname
            return
        dists.append(size_distribution(indiv[['size_class', 'num_individuals']],
            BINSIZE=BINSIZE, remove_last_bin=remove_last_bin))
        del(indiv)
    return sum(dists) / float(len(dists))

