import os
from collections import defaultdict, namedtuple
import abc

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as tkr
from scipy import stats

def _inches_to_mm(x):
    return x * 25.4

def mm_to_inches(x):
    return x / 25.4

def _format_thousands(x, pos):
    # careful with this
    s = '{:0,d}'.format(int(x))
    return s
_thousands_formatter = tkr.FuncFormatter(_format_thousands)

class _Journal(object):
    def __init__(self, name, size_one_column, size_two_columns):
        # Use maximum sizes
        self.column_sizes = {
                        1: size_one_column,
                        2: size_two_columns
                     }
    def get_column_size(self, num_columns):
        return self.column_sizes[num_columns]

_journal_a = _Journal('Nature',
        (mm_to_inches(89), mm_to_inches(250)),
        (mm_to_inches(183), mm_to_inches(250)))
_journal_b = _Journal('Proceedings of the Royal Society B: Biological Sciences',
        (mm_to_inches(84), mm_to_inches(250)),
        (mm_to_inches(175), mm_to_inches(250)))
_journal_c = _Journal('Ecology',
        (mm_to_inches(76), mm_to_inches(600)),
        (mm_to_inches(76), mm_to_inches(600)))
mpl.rcParams.update({'font.size': 9})

ParamPairType = namedtuple('ParamPair',
        ('theta', 'm', 'color', 'label', 'plotstyle'))
def ParamPair(theta, m, color=None, label=None, plotstyle=None):
    return ParamPairType(theta, m, color, label, plotstyle)

def param_pair_to_directory_name(param_pair, full_path=None, output=True):
    directory = os.path.normpath("theta_%s-m_%s" %
            (param_pair.theta, param_pair.m))
    if output:
        directory = os.path.join(directory, "output")
    if full_path:
        directory = os.path.join(full_path, directory)
    return directory

class Figure():
    __metaclass__ = abc.ABCMeta
    journals  = {
                    'a': _journal_a,
                    'b': _journal_b,
                    'c': _journal_c,
                }


    def __init__(self, size=None, num_columns=1, journal='c', subplot_id=None):
        if journal not in self.journals:
            print journal
            raise ValueError("Unknown journal.")
        if num_columns < 1 or num_columns > 2:
            raise ValueError("Number of columns is too high or too low.")
        max_size = self.journals[journal].get_column_size(num_columns)
        if not size:
            size = max_size
        else:
            if (size[0] and size[0] > max_size[0]) \
                or (max_size[1] and size[1] > max_size[1]):
                    raise ValueError("Figure size is too large.")
        if size[0] and not size[1]:
            size[1] = max_size[1]
        elif size[1] and not size[0]:
            size[0] = max_size[0]
        if not subplot_id:
            subplot_id = 111
        self.figure = plt.figure(figsize=size, dpi=600)
        if isinstance(subplot_id, list):
            self.ax = self.add_subplot(*subplot_id)
        else:
            self.ax = self.add_subplot(subplot_id)

        self.colors = ['#0000ff', '#ff1111', '#10b810']

    def savefig(self, filename):
        self.figure.savefig(filename, bbox_inches='tight', dpi=self.figure.dpi)
        print 'Saving with %d DPI' % self.figure.dpi

    def set_xlabel(self, label):
        self.ax.set_xlabel(label)

    def set_ylabel(self, label):
        self.ax.set_ylabel(label)

    def clf(self):
        self.figure.clf()

    def add_legend(self, plots, labels, *args, **kw):
        legend = plt.legend(plots, labels, *args, **kw)
        self.ax.add_artist(legend)

    def add_subplot(self, *args, **kw):
        ax = self.figure.add_subplot(*args, **kw)
        ax.xaxis.set_major_formatter(_thousands_formatter)
        ax.yaxis.set_major_formatter(_thousands_formatter)
        ax.xaxis.set_minor_formatter(_thousands_formatter)
        ax.yaxis.set_minor_formatter(_thousands_formatter)
        return ax

    @abc.abstractmethod
    def build_figure(self):
        pass


def bin_and_plot_pairs(individuals_gval_pairs, graph_filename, bin_fn=None,
        plot_fn=None, xlabel='', ylabel='', title='', clf=True, *args, **kw):
    if not bin_fn:
        do_nothing = lambda x: x
        bin_fn = do_nothing
    if not plot_fn:
        plot_fn = plt.plot
    individuals = defaultdict(list)
    for pair in individuals_gval_pairs:
        individuals[bin_fn(pair[0])].append(pair[1])

    num_individuals = []
    g_values = []
    for key in individuals:
        num_individuals.append(key)
        g_values.append(np.mean(individuals[key]))

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plot_fn(num_individuals, g_values, 'b.', *args, **kw)
    plt.savefig(graph_filename)
    if clf:
        plt.clf()


