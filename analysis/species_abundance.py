import numpy as np
from collections import Counter
from scipy import stats

### TODO: Preston binning

def species_abundance_distribution(species_abundances):
    dist = Counter(species_abundances)
    array_distribution = np.array([dist[i]
        for i in range(1, int(max(dist)) + 1)])
    return array_distribution

def max_length_in_list(arr_list):
    return max((len(arr) for arr in arr_list))

def normalize_array_lengths(arr_list, length=None):
    max_length = max_length_in_list(arr_list)
    len_adjusted_arrays =[np.append(arr, np.zeros(max_length - len(arr)))
        for arr in arr_list]
    return len_adjusted_arrays

def mean_species_abundance_distribution(species_abundance_distributions):
    '''
    species_abundance_distributions is a list of numpy arrays
    '''
    normed_sads = normalize_array_lengths(species_abundance_distributions)
    return sum(normed_sads) / len(normed_sads)

def distribution_chisq(observed, expected):
    observed, expected = normalize_array_lengths([observed, expected])
    expected_not_0 = expected != 0
    return stats.chisquare(observed[expected_not_0], expected[expected_not_0])
